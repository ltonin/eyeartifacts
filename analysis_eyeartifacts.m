%clear all; clc;
rootpath   = '/mnt/data/Research/eyeartifacts/';
subfolder  = '20150605_a1/';
subject    = 'a1';

FilesId    = {'All'};
basename  = [rootpath subfolder];

EVENTCUE = [16 17 18 19 20];
EVENTLBL = {'Left', 'Right', 'Top', 'Down', 'Blink'};
NumEvents = length(EVENTCUE);
EVENTEND = 32;
FiltBands = [2 30];
SampleRate = 512;
PeriodTime = 2;
PeriodSamples = PeriodTime*SampleRate;
SelChannels = [1 2 3 4 5 6];
NumSelChannels = length(SelChannels);

disp('[io] - Importing gdf files');
GDFfiles     = util_getfiles({basename}, '.gdf', 'offline', FilesId);
[s, h, info] = util_cat_gdf(GDFfiles(:));
NumFiles = length(GDFfiles);

LblEOG = zeros(size(s, 1), 1);
for evtId = 1:NumEvents
   ctyp = EVENTCUE(evtId);
   cpos = h.pos(h.typ == ctyp);
   for pId = 1:length(cpos)
       cstart = cpos(pId);
       cstop  = cpos(pId) + PeriodSamples - 1;
       LblEOG(cstart:cstop) = ctyp;
   end
end

LblFile = zeros(size(s, 1), 1);
for fId = 1:NumFiles
    startpos = h.pos(h.typ == 1);
    stoppos  = h.pos(h.typ == 32);
    
    cstart = startpos(find(startpos >= info.datasize(1, fId), 1, 'first'));
    cstop  = stoppos(find(stoppos < info.datasize(2, fId), 1, 'last'));
    LblFile(cstart:cstop) = fId;
end


% CAR
scar = proc_car(s);

% Bandpass
s_filt = (filt_bp(scar, 4, FiltBands, SampleRate));
%s_filt = proc_env(s_filt);



% v_eog = s_filt(:, ChannelCentrId) - mean(s_filt(:, [ChannelLeftId ChannelRightId]), 2);
% h_eog = s_filt(:, ChannelLeftId) - s_filt(:, ChannelRightId);


meanChan = zeros(NumSelChannels, 1);
meanFree = zeros(NumSelChannels, 1);
stdChan = zeros(NumSelChannels, 1);
stdFree = zeros(NumSelChannels, 1);


for evtId = 1:NumEvents
%     mVEOG(evtId) = mean(v_eog(LblEOG == EVENTCUE(evtId) & LblFile > 0, :));
%     mHEOG(evtId) = mean(h_eog(LblEOG == EVENTCUE(evtId) & LblFile > 0, :));
    for chId = 1:NumSelChannels
        meanChan(chId) = mean(s_filt(LblEOG == EVENTCUE(evtId) & LblFile > 0, SelChannels(chId)));
        stdChan(chId) = std(s_filt(LblEOG == EVENTCUE(evtId) & LblFile > 0, SelChannels(chId)));
        meanFree(chId) = mean(s_filt(LblEOG == 0 & LblFile > 0, SelChannels(chId)));
        stdFree(chId) = std(s_filt(LblEOG == 0 & LblFile > 0, SelChannels(chId)));
    end
end


