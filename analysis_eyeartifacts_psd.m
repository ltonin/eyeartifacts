clear all; clc;
rootpath   = '/mnt/data/Research/eyeartifacts/';
subfolder  = '20150616_a1/';
subject    = 'a1';

FilesId    = {'All'};
basename  = [rootpath subfolder];

WinShift    = 32;  % Samples
FreqGrid    = 0:2:256;
NumFreqs    = length(FreqGrid);
SelFreqLb   = 2:2:30;
[~, SelFreq] = intersect(FreqGrid, SelFreqLb);
SelChans    = [1 2 3 4 5 6];
%SelChans    = [1 3 4 5];
% SelChans    = [1];


EVENTCUE = [16 17 18 19 20];
EVENTLBL = {'Left', 'Right', 'Top', 'Down', 'Blink'};
EVENTFIX = 21;
NumEvents = length(EVENTCUE);
EVENTEND = 32;
FiltBands = [2 30];
SampleRate = 512;
PeriodTime = 2;
PeriodSamples = PeriodTime*SampleRate;

%% Importing GDFs
disp('[io] - Importing gdf files');
GDFfiles     = util_getfiles({basename}, '.gdf', 'offline', FilesId);
[s, h, info] = util_cat_gdf(GDFfiles(:));
NumFiles = length(GDFfiles);


%% Importing PSD files
disp('[io] - Importing pds files');
PSDfiles = util_getfiles({basename}, '.dat', 'offline', FilesId);
psd = [];
for fId = 1:length(PSDfiles)
    cpsd      = wc_load(PSDfiles{fId}, 'eigen', 'double');
    csamples  = size(cpsd, 1)/NumFreqs;
    cchannels = size(cpsd, 2);
    cpsd      = wc_2dTo3d(cpsd, [NumFreqs cchannels csamples]);
    psd       = cat(1, psd, permute(cpsd, [3 1 2]));
end
NumFrames = size(psd, 1);

%% SMR GDF Events extraction
disp('[proc] - Extracting event for SMR BCI');
evttyp      = h.typ;
evtpos      = ceil(h.pos/WinShift) + 1;

events = zeros(NumFrames, 1);
events(evtpos) = evttyp;

%% Spectral profile
NormFreq    = squeeze(psd(:, 1, :));
FreqProfile = squeeze(sum(psd(:, SelFreq, :), 2))./NormFreq;


%% Detection eye artifacts
th = 0:0.1:10;
EyeDetected = zeros(NumFrames, length(SelChans), length(th));
for wId = 1:NumFrames
    cnprof = squeeze(psd(wId, 1, SelChans));
    cfprof = squeeze(sum(psd(wId, SelFreq, SelChans), 2))./cnprof;
   % cfprof = log(cfprof);
    for ith = 1:length(th)
        id = find(cfprof > th(ith));

        if isempty(id)
            EyeDetected(wId, :, ith) = 0;
        else
            EyeDetected(wId, id, ith) = 1;
        end
    end
end

%% Check detection performances
DetectionPerf = zeros(length(EVENTCUE), length(th));

for evtId = 1:length(EVENTCUE)
    ctyp = EVENTCUE(evtId);
    cpos = evtpos(evttyp == ctyp);
    
    cdet = zeros(length(cpos),length(th));
    for pId = 1:length(cpos)
        cstart = cpos(pId);
        cstop  = cstart + floor(PeriodTime*SampleRate/WinShift) - 1;
         
        for thId = 1:length(th)
            cdet(pId, thId) = ~isempty(find(EyeDetected(cstart:cstop, :, thId), 1));
        end    
    end
    
    DetectionPerf(evtId, :) = 100*sum(cdet, 1)./length(cpos);
    
end


%% Check false positive
ctyp = EVENTFIX;
cpos = evtpos(evttyp == ctyp);
errdet = zeros(length(cpos),length(th));
for pId = 1:length(cpos)
        cstart = cpos(pId);
        cstop  = cstart + floor(PeriodTime*SampleRate/WinShift) - 1;
         
        for thId = 1:length(th)
            errdet(pId, thId) = ~isempty(find(EyeDetected(cstart:cstop, :, thId), 1));
        end        
end

ErrorDetection = 100*sum(errdet, 1)./length(cpos);


%% Plotting
fig = figure;
fig_set_position(fig, 'Top');
imagesc([DetectionPerf; ErrorDetection]); 
set(gca, 'XTick',0:5:length(th)); 
set(gca, 'XTickLabel', num2str(th(1:5:end)')); 
set(gca, 'YTick', 1:length(EVENTCUE) + 1);
set(gca, 'YTickLabel', {EVENTLBL{:}, 'Erroneous'});
plot_setCLim(gca, [0 100]);
xlabel('Thresholds');
ylabel('Events');
colorbar;
title(['Percentage of detected artifacts and erroneous detection. Channels ' num2str(SelChans)]);





